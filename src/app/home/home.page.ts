import { Platform } from '@ionic/angular';
import { Component } from '@angular/core';

import { WifiWizard2 } from '@ionic-native/wifi-wizard-2/ngx';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  ssid: string;
  scan: string;
  infotxt: string;
  btndisabled: string;
  gps: string;

  constructor(
    private platform: Platform,
    private wifiWizard2: WifiWizard2,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions
  ) {
    this.gps = 'deshabilitado';
    this.btndisabled = 'false';
    this.infotxt = 'Obtener redes';
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {

        this.checkGPSPermission();
        this.wifiWizard2.getConnectedSSID().then((datos) => {
          this.ssid = datos;
        });

      }
    });
    if (this.platform.is('cordova')) {
      console.log('cordova está disponible en el dispositivo');
    }
    else {
      console.log('cordova no está disponible en el dispositivo');
    }
  }

  async getNetworks() {
    console.log('buscando redes...');
    this.infotxt = 'Intentando obtener redes...';
    this.btndisabled = 'true';
    try {
      this.scan = await this.wifiWizard2.scan();
      this.infotxt = 'Obtener redes';
      this.btndisabled = 'false';
    } catch (error) {
      console.log(error);
    }
  }

  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          // If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          // If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log('4');
      } else {
        // Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              // Show alert if user click on 'No Thanks'
              this.gps = 'deshabilitado';
              alert('requestPermission Error requesting location permissions ' + error);
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.gps = 'habilitado';
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

}
