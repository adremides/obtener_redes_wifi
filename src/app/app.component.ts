import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.platform.is('cordova')) {
        console.log('es cordova');

        this.checkWifiWizzardRequiredPermission();

      }
    });
  }

  checkWifiWizzardRequiredPermission() {
    this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
    ).then(
      (result) => {
        if (!result.hasPermission) {
          console.log('no tengo el permiso, lo pido');
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then((permiso) => {
            console.log('Respuesta permiso:');
            console.log(permiso);
          });
        }
      }, (err) => {
        console.log(err);
        console.log('ERROR, no tengo el permiso, lo pido');
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
      }
    );
  }

}
